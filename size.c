/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   size.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 14:45:33 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/01 15:41:09 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

unsigned int	ft_size_great(t_elem *t, int ns, unsigned int i)
{
	if (ns == 1)
	{
		while (t)
		{
			if (i < t->nlink && ((t->elem_name)[0] != '.' || g_flag->a == '1'))
				i = t->nlink;
			t = t->next;
		}
	}
	else if (ns == 2)
	{
		while (t)
		{
			if (i < t->size && ((t->elem_name)[0] != '.' || g_flag->a == '1'))
			{
				i = t->size;
			}
			t = t->next;
		}
	}
	return (i);
}

unsigned int	ft_size_m(t_elem *t, int ns, unsigned int i)
{
	if (ns == 3)
	{
		while (t)
		{
			if (i < t->major)
				i = t->major;
			t = t->next;
		}
	}
	else if (ns == 4)
	{
		while (t)
		{
			if (i < t->minor)
				i = t->minor;
			t = t->next;
		}
	}
	return (i);
}

int				ft_size_greatest_number(t_elem *temp, int ns)
{
	unsigned int	i;
	unsigned int	j;
	t_elem			*t;

	t = temp;
	i = 0;
	j = 0;
	i = ft_size_great(t, ns, i);
	i = ft_size_m(t, ns, i);
	while (i > 0)
	{
		i = (i / 10);
		j++;
	}
	return (j);
}

int				ft_size_number(int i)
{
	int j;

	j = 0;
	if (i == 0)
		return (1);
	while (i > 0)
	{
		i = (i / 10);
		j++;
	}
	return (j);
}

void			ft_get_spaces_n(t_elem *t, t_elem *b, int n, unsigned int a)
{
	int i;
	int tmp;

	tmp = 0;
	tmp = (n == 1) ? (b->nlink) : (b->size);
	tmp = (n == 3) ? (b->major) : (tmp);
	tmp = (n == 4) ? (b->minor) : (tmp);
	if (a == 0)
		i = ft_size_greatest_number(t, n) - ft_size_number(tmp);
	else
		i = ft_size_number(a) - ft_size_number(tmp);
	while (i > 0)
	{
		ft_putchar(' ');
		i--;
	}
}
