/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 09:37:06 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/14 12:54:49 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int	static	ft_lenint(int n)
{
	int		count;

	count = 0;
	if (n == -2147483648)
		return (11);
	if (n <= 0)
	{
		n = -n;
		count++;
	}
	while (n > 0)
	{
		n = n / 10;
		count++;
	}
	return (count);
}

char		*ft_itoa(int n)
{
	char	*str;
	int		temp;
	int		i;

	temp = n;
	i = ft_lenint(n);
	str = (char *)malloc((i + 1) * sizeof(*str));
	if (str == NULL)
		return (NULL);
	if (n < 0 && n != -2147483648)
		temp = -n;
	else if (n == -2147483648)
		temp = 2147483647;
	str[i] = '\0';
	while (i > 0)
	{
		str[i - 1] = temp % 10 + '0';
		temp = temp / 10;
		i--;
	}
	if (n < 0)
		str[0] = '-';
	if (n == -2147483648)
		str[ft_lenint(n) - 1] = 8 + '0';
	return (str);
}
