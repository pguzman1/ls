/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 08:57:45 by pguzman           #+#    #+#             */
/*   Updated: 2015/11/26 10:00:41 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *ptr, int value, size_t num)
{
	size_t			i;
	unsigned char	b;
	unsigned char	*string;

	b = value;
	string = (unsigned char *)ptr;
	i = 0;
	while (i < num)
	{
		string[i] = value;
		i++;
	}
	return (ptr);
}
