/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 10:37:40 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/14 13:10:33 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		static		ft_nbrwrds(char c, const char *s)
{
	int	i;
	int	words;

	i = 0;
	words = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		if (s[i] != c && s[i])
			words++;
		while (s[i] != c && s[i])
			i++;
	}
	return (words);
}

char	static		*ft_strsave(char const *s, size_t *i, size_t *len, char c)
{
	while (s[*i] == c)
		(*i)++;
	while (s[*i] != c && s[*i])
	{
		(*i)++;
		(*len)++;
	}
	*i = *i - *len;
	return (ft_strsub(s, *i, *len));
}

char				**ft_strsplit(char const *s, char c)
{
	char		**res;
	size_t		i;
	size_t		j;
	size_t		words;
	size_t		len;

	words = 0;
	i = 0;
	j = 0;
	len = 0;
	words = ft_nbrwrds(c, s);
	res = (char **)malloc((words + 1) * sizeof(res));
	if (res == NULL)
		return (NULL);
	while (i < words)
	{
		res[i++] = ft_strsave(s, &j, &len, c);
		j = j + len;
		len = 0;
	}
	res[words] = NULL;
	return (res);
}
