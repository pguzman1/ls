/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m3.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:31:19 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:11:19 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_error	*ft_getmiddle_error(t_error *head)
{
	t_error	*slow;
	t_error	*fast;

	slow = head;
	fast = head;
	if (head == NULL)
		return (head);
	while (fast->next != NULL && fast->next->next != NULL)
	{
		slow = slow->next;
		fast = fast->next->next;
	}
	return (slow);
}

t_error	*ft_merge_error(t_error *a, t_error *b, \
		int (*cmp)(t_error *a, t_error *b))
{
	t_error	*dummyhead;
	t_error	*curr;
	t_error *res;

	dummyhead = malloc(sizeof(t_error));
	curr = dummyhead;
	while (a != NULL && b != NULL)
	{
		if (cmp(a, b) <= 0)
		{
			curr->next = a;
			a = a->next;
		}
		else
		{
			curr->next = b;
			b = b->next;
		}
		curr = curr->next;
	}
	curr->next = (a == NULL) ? b : a;
	res = dummyhead->next;
	free(dummyhead);
	return (res);
}

t_error	*ft_merge_sort_error(t_error *head, int (*cmp)(t_error *a, t_error *b))
{
	t_error	*middle;
	t_error	*shalf;

	if (head == NULL || head->next == NULL)
		return (head);
	middle = ft_getmiddle_error(head);
	shalf = middle->next;
	middle->next = NULL;
	return (ft_merge_error(ft_merge_sort_error(head, cmp), \
				ft_merge_sort_error(shalf, cmp), cmp));
}

t_elem	*ft_reverse(t_elem *curr)
{
	t_elem	*head;

	if (curr == NULL)
		return (NULL);
	if (curr->nextarg == NULL)
	{
		head = curr;
		return (head);
	}
	head = ft_reverse(curr->nextarg);
	curr->nextarg->nextarg = curr;
	curr->nextarg = NULL;
	return (head);
}

t_elem	*ft_reversenext(t_elem *curr)
{
	t_elem	*head;

	if (curr == NULL)
		return (NULL);
	if (curr->next == NULL)
	{
		head = curr;
		return (head);
	}
	head = ft_reversenext(curr->next);
	curr->next->next = curr;
	curr->next = NULL;
	return (head);
}
