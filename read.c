/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 10:48:17 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 17:16:50 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_elem	*ft_elem_error(char *path)
{
	struct stat	*buf;
	time_t		*p;
	t_elem		*error;

	error = malloc(sizeof(t_elem));
	error->elem_name = ft_strdup(".");
	error->lname = ft_strdup(path);
	error->next = malloc(sizeof(t_elem));
	buf = malloc(sizeof(struct stat));
	lstat(path, buf);
	p = malloc(sizeof(*p));
	p = ft_memcpy(p, &((buf->st_mtimespec).tv_sec), sizeof(*p));
	error->time = p;
	error->permissions = ft_strdup("d---------");
	error->nextarg = NULL;
	error->hidden = '2';
	error->user_name = NULL;
	error->group_name = NULL;
	free(buf);
	return (error);
}

t_elem	*ft_readdir(DIR *dire, t_elem *beg, char *path, t_elem *elem)
{
	struct dirent	*ditto;
	struct stat		*buf;
	char			*del;
	char			*del2;

	while ((ditto = readdir(dire)) != NULL)
	{
		buf = malloc(sizeof(struct stat));
		del = ft_strjoin(path, "/");
		del2 = ft_strjoin(del, ditto->d_name);
		if (lstat(del2, buf) == -1)
		{
			elem = ft_elem_error(ditto->d_name);
			beg = elem;
		}
		else if (!beg)
		{
			elem = ft_new_elem(*buf, ditto->d_name, path);
			beg = elem;
		}
		else
		{
			elem->next = ft_new_elem(*buf, ditto->d_name, path);
			elem = elem->next;
		}
		free(del);
		free(del2);
		free(buf);
	}
	return (beg);
}

int		ft_link(char *path)
{
	char	slink[256];

	return (readlink(path, slink, 256));
}

t_elem	*ft_read(char *path)
{
	t_elem		*elem;
	t_elem		*beg;

	elem = NULL;
	beg = NULL;
	return (ft_opendir(path, elem, beg));
}
