/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls7.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:18:42 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:14:08 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*ft_arg(char **argv, int i)
{
	if (i == 0 || ((ft_strcmp("--", argv[i]) == 0) && argv[i + 1] == NULL \
				&& ft_strcmp(argv[i - 1], "--") != 0))
		return (".");
	else if (ft_strcmp(argv[i - 1], "--") != 0 && argv[i][0] == '-' \
			&& (argv[i][1] == 'l' || argv[i][1] == 'R' || argv[i][1] == 'a' \
				|| argv[i][1] == '1' || argv[i][1] == 'r' || argv[i][1] == 't'))
		return (".");
	else
		return (argv[i]);
}

void	ft_easy(t_elem **start, t_elem **files, t_elem **folders)
{
	if (*start != NULL)
	{
		if ((*start)->nextarg != NULL)
			*start = ft_merge_sort(*start, ft_filedir);
		if ((*start)->next != NULL || (*start)->hidden == '2')
		{
			*files = NULL;
			*folders = *start;
		}
		else
		{
			*files = *start;
			while ((*start)->nextarg != NULL)
			{
				if ((*start)->nextarg->next == NULL)
					*start = (*start)->nextarg;
				else
					break ;
			}
			*folders = (*start)->nextarg;
			(*start)->nextarg = NULL;
		}
	}
}

void	ft_tem(t_elem **start, t_elem **beg)
{
	t_elem *temp;

	temp = NULL;
	temp = *start;
	while (temp->nextarg)
		temp = temp->nextarg;
	temp->nextarg = *beg;
}

void	ft_get(char **argv, t_error **list, t_elem **beg, t_elem **start)
{
	*beg = ft_read(*argv);
	if (*beg == NULL)
		*list = ft_readerror(*list, *argv);
	if (*beg != NULL)
		*beg = ft_begsort(*beg);
	if (*beg != NULL)
	{
		if (*start == NULL)
			*start = *beg;
		else
			ft_tem(start, beg);
	}
}

void	ft_compact(char **argv, t_elem **start, int i)
{
	t_error	*list;
	t_elem	*beg;

	list = NULL;
	beg = NULL;
	while (argv[i])
		ft_get(&(argv[i++]), &list, &beg, start);
	ft_openerror(list);
}
