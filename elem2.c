/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elem2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 11:46:55 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:01:49 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_fill_elem(t_elem **elem, struct stat buf, char name[256])
{
	(*elem)->user_name = getpwuid(buf.st_uid)->pw_name;
	(*elem)->group_name = getgrgid(buf.st_gid)->gr_name;
	(*elem)->elem_name = ft_strdup(name);
	(*elem)->hidden = ((*elem)->elem_name[0] == '.') ? ('1') : ('0');
	(*elem)->permissions = ft_permissions(buf);
	(*elem)->nlink = (int)buf.st_nlink;
	(*elem)->size = (int)buf.st_size;
	(*elem)->minor = (((*elem)->permissions[0]) == 'c' || \
			((*elem)->permissions)[0] == 'b') ? (minor(buf.st_rdev)) : (0);
	(*elem)->major = (((*elem)->permissions[0]) == 'c' || \
			((*elem)->permissions)[0] == 'b') ? (major(buf.st_rdev)) : (0);
	(*elem)->blocks = (int)buf.st_blocks;
	(*elem)->next = NULL;
	(*elem)->nextarg = NULL;
}

void	ft_sym_name(t_elem **elem)
{
	char	*t;
	char	*temp;

	temp = ft_strjoin("/", (*elem)->elem_name);
	if (((*elem)->permissions)[0] == 'l')
		t = ft_strjoin((*elem)->lname, (((*elem)->lname)[ft_strlen((*elem)->\
					lname) - 1] == '/') ? ((*elem)->elem_name) : (temp));
	(*elem)->symlink_name = (((*elem)->permissions)[0] == 'l') ? \
							(ft_get_symlnk(t)) : (NULL);
	if ((((*elem)->permissions))[0] == 'l')
		free(t);
	free(temp);
}
