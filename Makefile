# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pguzman <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/25 09:31:23 by pguzman           #+#    #+#              #
#    Updated: 2016/02/03 11:52:19 by pguzman          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= ft_ls

SRC		= ft_ls.c elem.c read.c merge.c print.c size.c ls1.c ls2.c ls3.c ls4.c \
		  ls5.c ls6.c ls7.c m1.c m2.c m3.c size2.c read2.c free.c elem2.c

OBJ		= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -Werror

$(NAME): $(OBJ)
		@make -C libft/
			@gcc $(OBJ) -o $(NAME) -L libft/ -lft

all: $(NAME)

clean:
		@make -C libft/ clean
			@rm -rf $(OBJ)

fclean: clean
		@rm -rf $(NAME) $(OBJ)

re: fclean $(NAME)
