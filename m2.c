/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:33:21 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:10:37 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_elem	*ft_merge(t_elem *a, t_elem *b, int (*cmp)(t_elem *a, t_elem *b))
{
	t_elem	*dummyhead;
	t_elem	*curr;
	t_elem	*res;

	dummyhead = malloc(sizeof(t_elem));
	curr = dummyhead;
	while (a != NULL && b != NULL)
	{
		if (cmp(a, b) <= 0)
		{
			curr->nextarg = a;
			a = a->nextarg;
		}
		else
		{
			curr->nextarg = b;
			b = b->nextarg;
		}
		curr = curr->nextarg;
	}
	curr->nextarg = (a == NULL) ? b : a;
	res = dummyhead->nextarg;
	free(dummyhead);
	return (res);
}

t_elem	*ft_merge_sort(t_elem *head, int (*cmp)(t_elem *a, t_elem *b))
{
	t_elem	*middle;
	t_elem	*shalf;

	if (head == NULL || head->nextarg == NULL)
		return (head);
	middle = ft_getmiddle(head);
	shalf = middle->nextarg;
	middle->nextarg = NULL;
	return (ft_merge(ft_merge_sort(head, cmp), ft_merge_sort(shalf, cmp), cmp));
}

t_elem	*ft_getmiddlenext(t_elem *head)
{
	t_elem	*slow;
	t_elem	*fast;

	slow = head;
	fast = head;
	if (head == NULL)
		return (head);
	while (fast->next != NULL && fast->next->next != NULL)
	{
		slow = slow->next;
		fast = fast->next->next;
	}
	return (slow);
}

t_elem	*ft_mergenext(t_elem *a, t_elem *b, int (*cmp)(t_elem *a, t_elem *b))
{
	t_elem	*dummyhead;
	t_elem	*curr;
	t_elem	*res;

	dummyhead = malloc(sizeof(t_elem));
	curr = dummyhead;
	while (a != NULL && b != NULL)
	{
		if (cmp(a, b) <= 0)
		{
			curr->next = a;
			a = a->next;
		}
		else
		{
			curr->next = b;
			b = b->next;
		}
		curr = curr->next;
	}
	curr->next = (a == NULL) ? b : a;
	res = dummyhead->next;
	free(dummyhead);
	return (res);
}

t_elem	*ft_merge_sortnext(t_elem *head, int (*cmp)(t_elem *a, t_elem *b))
{
	t_elem	*middle;
	t_elem	*shalf;
	t_elem	*savearg;
	t_elem	*res;

	if (head == NULL || head->next == NULL)
		return (head);
	middle = ft_getmiddle(head);
	savearg = head->nextarg;
	shalf = middle->next;
	middle->next = NULL;
	res = ft_mergenext(ft_merge_sortnext(head, cmp), \
			ft_merge_sortnext(shalf, cmp), cmp);
	res->nextarg = savearg;
	return (res);
}
