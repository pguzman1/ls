/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/23 15:53:10 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 17:10:44 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include <stdio.h>

t_flag	*g_flag;

void	ft_print_time(time_t *s)
{
	int		i;
	char	*str;
	int		stop;

	i = 4;
	stop = 16;
	str = ctime(s);
	if (time(NULL) - *s < 0)
	{
		ft_putstr("Jan  1  10000");
		return ;
	}
	if (time(NULL) - *s > 15724800)
		stop = 11;
	while (i != stop)
		ft_putchar(str[i++]);
	if (stop == 11)
	{
		ft_putchar(' ');
		i = 20;
		while (i < 24)
			ft_putchar(str[i++]);
	}
}

int		ft_get_total(t_elem *t)
{
	int	i;

	i = 0;
	while (t)
	{
		if ((t->elem_name)[0] != '.' || g_flag->a == '1')
			i = i + t->blocks;
		t = t->next;
	}
	return (i);
}

char	ft_both(t_elem *r)
{
	t_elem	*t;
	char	a;
	char	b;

	a = '0';
	b = '0';
	t = r;
	while (t)
	{
		if ((t->permissions)[0] != 'c' && (t->permissions)[0] != 'b')
			a = '1';
		else
			b = '1';
		t = t->next;
		if (a == '1' && b == '1')
			return ('1');
	}
	return ('0');
}

char	ft_just_points(t_elem *r)
{
	t_elem	*t;

	t = r;
	while (t)
	{
		if ((t->elem_name)[0] != '.')
			return ('0');
		t = t->next;
	}
	return ('1');
}

int		main(int argc, char **argv)
{
	t_elem		*start;
	t_elem		*files;
	t_elem		*folders;
	int			i;

	start = NULL;
	files = NULL;
	folders = NULL;
	ft_start_flags();
	i = ft_flags(argc, argv);
	argc = argc - i;
	argv[i] = ft_arg(argv, i);
	ft_compact(argv, &start, i);
	ft_easy(&start, &files, &folders);
	ft_fi(files, start, folders, argc);
	ft_main_folders(folders, start, argc);
	ft_free();
//	ft_free_files(&files);
//	ft_free_folders(&folders);
	return (0);
}
