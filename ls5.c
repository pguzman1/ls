/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:21:32 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 17:47:41 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*ft_get_symlnk(char *av)
{
	char	*str;
	int		len;
	char	slink[256];

	len = readlink(av, slink, 256);
	if (errno == EINVAL)
	{
		errno = 0;
		return (NULL);
	}
	str = (char *)malloc(sizeof(char) * (len));
	str[len] = '\0';
	ft_strncpy(str, slink, len);
	return (str);
}

t_elem	*ft_tm_sort(t_elem *tm)
{
	t_elem *del;
	
	del = tm;
	if (tm->hidden != '2')
		ft_merge_sortnext(tm, ft_alpha);
	if (g_flag->t == '1' && tm->hidden != '2')
		tm = ft_merge_sortnext(tm, ft_tttime);
	if (g_flag->r == '1' && tm->hidden != '2')
		tm = ft_reversenext(tm);
	if (tm->hidden != '2')
		ft_ls_l(tm, 1);
	return (tm);
}

void	ft_rcore(t_elem **startt, t_elem *tm, char *f)
{
	char	*del;
	t_elem	*tmdel;

	del = ft_strjoin("/", (*startt)->elem_name);
	if (ft_strcmp("/", (*startt)->lname) != 0)
		f = (ft_strjoin((*startt)->lname,del));
	else
		f = del;
	ft_putchar('\n');
	ft_putstr(f);
	ft_putstr(":\n");
	tm = ft_read(f);
	while (tm == NULL || tm->hidden == '2')
	{
		ft_putstr("ls :");
		opendir((*startt)->elem_name);
		perror((*startt)->elem_name);
		free(tm->elem_name);
		free(tm->lname);
		free(tm->permissions);
		free(tm->time);
		tmdel = tm;
		if (tm != NULL)
		{
			tm = tm->nextarg;
			free(tmdel);
		}
		if (tm == NULL)
			break;
	}
	if (tm != NULL)
		tm = ft_tm_sort(tm);
	ft_putstr("free1\n");
	free((*startt)->elem_name);
	free((*startt)->lname);
	free((*startt)->permissions);
	free((*startt)->time);
	((*startt)->symlink_name) ? (free((*startt)->symlink_name)) : (NULL);
	free(del);
	free(f);
}

void	ft_r(t_elem *start)
{
	char	*f;
	t_elem	*tm;
	t_elem	*startt;

	startt = start;
	tm = NULL;
	f = NULL;
	while (startt)
	{
		if ((ft_strcmp(startt->elem_name, ".") != 0) && \
				ft_strcmp(startt->elem_name, "..") && \
				(startt->permissions)[0] == 'd' \
				&& (startt->elem_name[0] != '.' || g_flag->a == '1'))
		{
			ft_rcore(&startt, tm, f);
			free(f);
			startt = startt->next;
		}
		else
		{
			free(startt->elem_name);
			free(startt->lname);
			free(startt->permissions);
			free(startt->time);
			(startt->symlink_name) ? (free(startt->symlink_name)) : (NULL);
			startt = startt->next;
		}
	}
}

void	ft_fi(t_elem *files, t_elem *start, t_elem *folders, int acc)
{
	t_elem *temp;

	temp = NULL;
	if (files && files->nextarg)
	{
		if (g_flag->t == '0')
			files = ft_merge_sort(files, ft_alpha);
		else
			files = ft_merge_sort(files, ft_tttime);
	}
	if (files)
	{
		if (g_flag->r == '1')
			files = ft_reverse(files);
		start = files;
		while (start)
		{
			temp = start;
			temp = ft_ls_l(start, acc);
			start = start->nextarg;
		}
		if (folders)
			ft_putchar('\n');
	}
}
