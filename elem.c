/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elem.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/31 09:36:29 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 16:17:53 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_elem	*ft_putelemfile(t_elem *elem, struct stat buf, char *path, time_t *p)
{
	char	*tmp;
	char	*tmp2;

	elem->user_name = getpwuid(buf.st_uid)->pw_name;
	elem->group_name = getgrgid(buf.st_gid)->gr_name;
	elem->lname = ft_strdup(path);
	elem->elem_name = ft_strdup(path);
	elem->time = p;
	tmp = ft_strrchr(path, '/');
	tmp2 = (!(tmp)) ? (path) : (tmp + 1);
	elem->hidden = ((tmp2)[0] == '.') ? ('1') : ('0');
	elem->permissions = ft_permissions(buf);
	elem->nlink = (int)buf.st_nlink;
	elem->size = (int)buf.st_size;
	elem->symlink_name = ((elem->permissions)[0] == 'l') ? \
	(ft_get_symlnk(elem->lname)) : (NULL);
	elem->minor = ((elem->permissions[0]) == 'c' || \
			(elem->permissions)[0] == 'b') ? (minor(buf.st_rdev)) : (0);
	elem->major = ((elem->permissions[0]) == 'c' || \
			(elem->permissions)[0] == 'b') ? (major(buf.st_rdev)) : (0);
	elem->blocks = (int)buf.st_blocks;
	elem->next = NULL;
	elem->nextarg = NULL;
	return (elem);
}

t_elem	*ft_new_elemfile(char *path)
{
	t_elem		*elem;
	struct stat	*buf;
	time_t		*p;

	buf = malloc(sizeof(struct stat));
	lstat(path, buf);
	p = malloc(sizeof(*p));
	p = ft_memcpy(p, &((buf->st_mtimespec).tv_sec), sizeof(*p));
	elem = malloc(sizeof(t_elem));
	elem = ft_putelemfile(elem, *buf, path, p);
	free(buf);
	return (elem);
}

t_elem	*ft_putelem(struct stat buf, char name[256], t_elem *elem)
{
	ft_fill_elem(&elem, buf, name);
	ft_sym_name(&elem);
	return (elem);
}

t_elem	*ft_new_elem(struct stat buf, char name[256], char *longname)
{
	t_elem	*elem;
	time_t	*p;

	p = malloc(sizeof(*p));
	p = ft_memcpy(p, &((buf.st_mtimespec).tv_sec), sizeof(*p));
	elem = malloc(sizeof(t_elem));
	elem->time = p;
	elem->lname = ft_strdup(longname);
	ft_putelem(buf, name, elem);
	return (elem);
}

void	ft_add_elem(t_elem **beg, struct stat buf, char name[256], char *lname)
{
	t_elem	*temp;

	temp = *beg;
	if (!*beg)
	{
		*beg = ft_new_elem(buf, name, lname);
	}
	else
	{
		while (temp->next != NULL)
			temp = temp->next;
		temp->next = ft_new_elem(buf, name, lname);
	}
}
