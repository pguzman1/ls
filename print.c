/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/31 09:37:16 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:00:11 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_print_date(char *s)
{
	int i;

	i = 4;
	while (i < 16)
		ft_putchar(s[i++]);
}

char	*ft_filetype(char *s, struct stat buf)
{
	if (S_ISBLK(buf.st_mode))
		s[0] = 'b';
	else if (S_ISLNK(buf.st_mode))
		s[0] = 'l';
	else if (S_ISREG(buf.st_mode))
		s[0] = '-';
	else if (S_ISFIFO(buf.st_mode))
		s[0] = 'p';
	else if (S_ISCHR(buf.st_mode))
		s[0] = 'c';
	else if (S_ISDIR(buf.st_mode))
		s[0] = 'd';
	else if (S_ISSOCK(buf.st_mode))
		s[0] = 's';
	return (s);
}

char	*ft_per_user(struct stat buf, char *per)
{
	if (buf.st_mode & S_IRUSR)
		per[1] = 'r';
	if (buf.st_mode & S_IWUSR)
		per[2] = 'w';
	if (buf.st_mode & S_IXUSR)
		per[3] = (buf.st_mode & S_ISUID) ? 's' : 'x';
	else
		per[3] = (buf.st_mode & S_ISUID) ? 'S' : '-';
	return (per);
}

char	*ft_permissions(struct stat buf)
{
	char	*per;

	per = ft_strdup("----------");
	per = ft_per_user(buf, per);
	if (buf.st_mode & S_IRGRP)
		per[4] = 'r';
	if (buf.st_mode & S_IWGRP)
		per[5] = 'w';
	if (buf.st_mode & S_IXGRP)
		per[6] = (buf.st_mode & S_ISGID) ? 's' : 'x';
	else
		per[6] = (buf.st_mode & S_ISGID) ? 'S' : '-';
	if (buf.st_mode & S_IROTH)
		per[7] = 'r';
	if (buf.st_mode & S_IWOTH)
		per[8] = 'w';
	if (buf.st_mode & S_IXOTH)
		per[9] = (buf.st_mode & S_ISTXT) ? 't' : 'x';
	else
		per[9] = (buf.st_mode & S_ISTXT) ? 'T' : '-';
	return (ft_filetype(per, buf));
}
