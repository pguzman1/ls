/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:21:17 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:10:23 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_alpha(t_elem *elem1, t_elem *elem2)
{
	return (ft_strcmp(elem1->elem_name, elem2->elem_name));
}

int		ft_alphaerror(t_error *elem1, t_error *elem2)
{
	return (ft_strcmp(elem1->path, elem2->path));
}

int		ft_alphadir(t_elem *elem1, t_elem *elem2)
{
	return (ft_strcmp(elem1->lname, elem2->lname));
}

int		ft_tttime(t_elem *elem1, t_elem *elem2)
{
	if (*(elem1->time) < *(elem2->time))
		return (1);
	else if (*(elem1->time) == *(elem2->time))
		return (ft_strcmp(elem1->elem_name, elem2->elem_name));
	else
		return (0);
}

time_t	*ft_timefolder(t_elem *elem1)
{
	t_elem	*t;

	t = elem1;
	while (ft_strcmp(t->elem_name, ".") != 0)
		t = t->next;
	return (t->time);
}
