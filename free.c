/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 11:29:04 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 11:00:31 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_free(void)
{
	free(g_flag);
}

void	ft_free_files(t_elem	**files)
{
	t_elem	*temp;
	while (*files)
	{
		temp = *files;
		((*files)->permissions) ? free((*files)->permissions): NULL;
		((*files)->time) ? free((*files)->time): NULL;
		
		((*files)->symlink_name) ? free((*files)->symlink_name): NULL;
		
	((*files)->lname) ? free((*files)->lname): NULL;
	((*files)->elem_name) ? free((*files)->elem_name): NULL;
		*files = (*files)->nextarg;
		free(*files);
	}
}

void	ft_free_folders(t_elem **folders)
{
	t_elem	*temp;
//	t_elem	*t;
	while (*folders)
	{
		temp = *folders;
		*folders = (*folders)->nextarg;
		free(temp);
	/*	while (temp)
		{
			t = temp;
			((temp)->permissions) ? free((temp)->permissions): NULL;
			((temp)->time) ? free((temp)->time): NULL;
			((temp)->symlink_name) ? free((temp)->symlink_name): NULL;
			temp = temp->next;
			free(t);
		}*/
	}
}
