/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls2.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:26:49 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/03 13:43:12 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_gmj(t_elem *beg)
{
	t_elem			*t;
	unsigned int	i;

	i = 0;
	t = beg;
	while (t)
	{
		if (i < t->major)
			i = t->major;
		t = t->nextarg;
	}
	return (i);
}

int		ft_gmn(t_elem *beg)
{
	t_elem			*t;
	unsigned int	i;

	i = 0;
	t = beg;
	while (t)
	{
		if (i < t->minor)
			i = t->minor;
		t = t->nextarg;
	}
	return (i);
}

void	ft_print_spaces(int a)
{
	int		i;

	i = 0;
	while (i < a)
	{
		ft_putchar(' ');
		i++;
	}
}

void	ft_l1(unsigned int great[7], t_elem *beg, t_elem *temp)
{
	if (great[0] == 0 && !(beg->next))
	{
		great[0] = ft_gnk(temp);
		great[1] = ft_gus(temp);
		great[2] = ft_ggr(temp);
		great[3] = ft_gsi(temp);
		great[4] = ft_gmj(temp);
		great[5] = ft_gmn(temp);
		great[6] = ft_botht(temp);
	}
	if (beg->next)
	{
		great[0] = 0;
		great[1] = 0;
		great[2] = 0;
		great[3] = 0;
		great[4] = 0;
		great[5] = 0;
		great[6] = 0;
	}
}

void	ft_l2(t_elem *beg, int elem, t_elem *temp)
{
	if (beg->next)
	{
		if (elem > 1)
		{
			ft_putstr(beg->lname);
			ft_putchar(':');
			ft_putchar('\n');
		}
		if (beg->hidden != '2')
		{
			if (((g_flag->l == '1' && ((ft_just_points(beg) == '0') \
								|| g_flag->a == '1')) || (g_flag->rr == '1' \
								&& g_flag->l == '1' \
								&& ft_just_points(beg) == '0')))
			{
				ft_putstr("total ");
				ft_putnbr(ft_get_total(temp));
				ft_putchar('\n');
			}
		}
	}
}
