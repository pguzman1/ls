/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/28 11:06:31 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 17:44:01 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# define FT_NAME printf("\033[32;1m%s\033[0m\n", __FUNCTION__);

# include <dirent.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/stat.h>
# include "libft/libft.h"
# include <pwd.h>
# include <time.h>
# include <grp.h>
# include <errno.h>
# include <sys/types.h>
# include <sys/xattr.h>
# include <sys/acl.h>
# include <stdio.h>

typedef struct		s_elem
{
	char				*permissions;
	char				*user_name;
	char				*group_name;
	time_t				*time;
	char				*elem_name;
	char				*lname;
	char				*symlink_name;
	char				hidden;
	unsigned int		nlink;
	unsigned int		size;
	int					blocks;
	struct s_elem		*next;
	struct s_elem		*nextarg;
	unsigned int		major;
	unsigned int		minor;

}					t_elem;

typedef struct		s_error
{
	char			*path;
	struct s_error	*next;
}					t_error;

typedef struct		s_flag
{
	char	l;
	char	rr;
	char	a;
	char	r;
	char	t;
	char	one;
}					t_flag;

t_elem				*ft_putelemfile(t_elem *elem, struct stat buf, char *path, \
		time_t *p);
t_elem				*ft_new_elemfile(char *path);
t_elem				*ft_putelem(struct stat buf, char name[256], t_elem *elem);
t_elem				*ft_new_elem(struct stat buf, char name[256], \
		char *longname);
void				ft_add_elem(t_elem **beg, struct stat buf, char name[256], \
		char *lname);
void				ft_print_time(time_t *s);
int					ft_get_total(t_elem *t);
char				ft_both(t_elem *r);
char				ft_just_points(t_elem *r);
int					ft_gnk(t_elem *beg);
int					ft_gsi(t_elem *beg);
int					ft_ggr(t_elem *beg);
int					ft_gus(t_elem *beg);
int					ft_botht(t_elem *beg);
int					ft_gmj(t_elem *beg);
int					ft_gmn(t_elem *beg);
void				ft_print_spaces(int a);
void				ft_l1(unsigned int great[7], t_elem *beg, t_elem *temp);
void				ft_l2(t_elem *beg, int elem, t_elem *temp);
void				ft_l3(t_elem *beg, t_elem *temp, unsigned int great[7]);
void				ft_l4(t_elem *beg, t_elem *temp, unsigned int great[7], \
		t_elem *r);
void				ft_l5(t_elem *beg);
void				ft_l_hidden(t_elem *beg);
void				ft_l6(t_elem *beg, t_elem *temp, unsigned int great[7], \
		t_elem *r);
t_elem				*ft_ls_l(t_elem *beg, int elem);
char				ft_set_flags(char c);
void				ft_start_flags(void);
void				ft_inf(char c);
int					ft_flags(int ac, char **av);
char				*ft_get_symlnk(char *av);
t_elem				*ft_tm_sort(t_elem *tm);
void				ft_rcore(t_elem **startt, t_elem *tm, char *f);
void				ft_r(t_elem *start);
void				ft_fi(t_elem *files, t_elem *start, t_elem *folders, \
		int acc);
void				ft_main_folders(t_elem *folders, t_elem *start, int acc);
void				ft_openerror(t_error *list);
t_error				*ft_readerror(t_error *list, char *str);
t_elem				*ft_begsort(t_elem *beg);
void				ft_list_elem(t_elem *start, t_elem *beg);
char				*ft_arg(char **argv, int i);
void				ft_easy(t_elem **start, t_elem **files, t_elem **folders);
void				ft_tem(t_elem **start, t_elem **beg);
void				ft_get(char **argv, t_error **list, t_elem **beg, \
		t_elem **start);
void				ft_compact(char **argv, t_elem **start, int i);

int					ft_alpha(t_elem *elem1, t_elem *elem2);
int					ft_alphaerror(t_error *elem1, t_error *elem2);
int					ft_alphadir(t_elem *elem1, t_elem *elem2);
int					ft_tttime(t_elem *elem1, t_elem *elem2);
time_t				*ft_timefolder(t_elem *elem1);
int					ft_tttime2(t_elem *elem1, t_elem *elem2);
int					ft_filedir(t_elem *elem1, t_elem *elem2);
t_elem				*ft_getmiddle(t_elem *head);
t_elem				*ft_merge(t_elem *a, t_elem *b, \
		int (*cmp)(t_elem *a, t_elem *b));
t_elem				*ft_merge_sort(t_elem *head, int (*cmp)(t_elem *a, \
			t_elem *b));
t_elem				*ft_getmiddlenext(t_elem *head);
t_elem				*ft_mergenext(t_elem *a, t_elem *b, \
		int (*cmp)(t_elem *a, t_elem *b));
t_elem				*ft_merge_sortnext(t_elem *head, \
		int (*cmp)(t_elem *a, t_elem *b));
t_error				*ft_getmiddle_error(t_error *head);
t_error				*ft_merge_error(t_error *a, t_error *b, \
		int (*cmp)(t_error *a, t_error *b));
t_error				*ft_merge_sort_error(t_error *head, \
		int (*cmp)(t_error *a, t_error *b));
t_elem				*ft_reverse(t_elem *curr);
t_elem				*ft_reversenext(t_elem *curr);

void				ft_print_date(char *s);
char				*ft_filetype(char *s, struct stat buf);
char				*ft_per_user(struct stat buf, char *per);
char				*ft_permissions(struct stat buf);

t_elem				*ft_elem_error(char *path);
t_elem				*ft_readdir(DIR *dire, t_elem *beg, char *path, \
		t_elem *elem);
t_elem				*ft_close(t_elem *elem, char *path, t_elem *beg, DIR *dire);
int					ft_link(char *path);
t_elem				*ft_opendir(char *path, t_elem *elem, t_elem *beg);
t_elem				*ft_read(char *path);

unsigned int		ft_size_great(t_elem *t, int ns, unsigned int i);
unsigned int		ft_size_m(t_elem *t, int ns, unsigned int i);
int					ft_size_greatest_number(t_elem *temp, int ns);
int					ft_size_number(int i);
void				ft_get_spaces_n(t_elem *t, t_elem *b, int n, \
		unsigned int a);
int					ft_size_greatest_str(t_elem *temp, int ns);
void				ft_get_spaces_str(t_elem *te, char *str, int ns, \
		unsigned int a);

extern	t_flag		*g_flag;
void				ft_free(void);
void				ft_fill_elem(t_elem **elem, struct stat buf, \
		char name[256]);
void				ft_sym_name(t_elem **elem);
void				ft_free_files(t_elem **files);
void				ft_free_folders(t_elem **folders);

# define MINORBITS       20
# define MINORMASK       ((1U << MINORBITS) - 1)

# define MAJOR(dev)      ((unsigned int) ((dev) >> MINORBITS))
# define MINOR(dev)      ((unsigned int) ((dev) & MINORMASK))
#endif
