/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls1.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:28:53 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/03 13:43:06 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_gnk(t_elem *beg)
{
	t_elem			*t;
	unsigned int	i;

	i = 0;
	t = beg;
	while (t)
	{
		if (i < t->nlink)
			i = t->nlink;
		t = t->nextarg;
	}
	return (i);
}

int		ft_gsi(t_elem *beg)
{
	t_elem			*t;
	unsigned int	i;

	i = 0;
	t = beg;
	while (t)
	{
		if (i < t->size)
			i = t->size;
		t = t->nextarg;
	}
	return (i);
}

int		ft_ggr(t_elem *beg)
{
	t_elem			*t;
	unsigned int	i;

	i = 0;
	t = beg;
	while (t)
	{
		if (i < ft_strlen(t->group_name))
			i = ft_strlen(t->group_name);
		t = t->nextarg;
	}
	return (i);
}

int		ft_gus(t_elem *beg)
{
	t_elem			*t;
	unsigned int	i;

	i = 0;
	t = beg;
	while (t)
	{
		if (i < ft_strlen(t->user_name))
			i = ft_strlen(t->user_name);
		t = t->nextarg;
	}
	return (i);
}

int		ft_botht(t_elem *beg)
{
	t_elem	*t;
	char	a;
	char	b;

	a = '0';
	b = '0';
	t = beg;
	while (t)
	{
		if ((t->permissions)[0] == 'c' || (t->permissions)[0] == 'b')
			a = '1';
		else
			b = '1';
		if (a == '1' && b == '1')
			return (1);
		t = t->nextarg;
	}
	return (0);
}
