/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls3.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:25:14 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 15:22:13 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_l3(t_elem *beg, t_elem *temp, unsigned int great[7])
{
	ft_putstr((beg)->permissions);
	ft_putstr("  ");
	ft_get_spaces_n(temp, beg, 1, great[0]);
	ft_putnbr((beg)->nlink);
	ft_putchar(' ');
	ft_putstr(beg->user_name);
	ft_get_spaces_str(temp, beg->user_name, 1, great[1]);
	ft_putstr("  ");
	ft_putstr((beg)->group_name);
	ft_get_spaces_str(temp, beg->group_name, 2, great[2]);
}

void	ft_l4(t_elem *beg, t_elem *temp, unsigned int great[7], t_elem *r)
{
	if ((beg->permissions)[0] != 'c' && (beg->permissions[0] != 'b'))
		ft_get_spaces_n(temp, beg, 2, great[3]);
	if ((beg->permissions)[0] != 'c' && (beg->permissions[0] != 'b'))
	{
		if (ft_both(r) == '1' || great[6] == 1)
		{
			ft_print_spaces(8 - ft_size_number(great[3]));
			if (great[3] != 0)
				ft_putchar(' ');
		}
		ft_putstr("  ");
		ft_putnbr((beg)->size);
	}
	else
	{
		ft_get_spaces_n(temp, beg, 3, great[4]);
		ft_putstr("   ");
		ft_putnbr(beg->major);
		ft_putstr(", ");
		ft_get_spaces_n(temp, beg, 4, great[3]);
		ft_putnbr(beg->minor);
	}
}

void	ft_l5(t_elem *beg)
{
	ft_putstr((beg)->elem_name);
	if (g_flag->l == '1')
	{
		if ((beg->permissions)[0] == 'l')
		{
			ft_putstr(" -> ");
			if (beg->symlink_name != NULL)
				ft_putstr(beg->symlink_name);
		}
	}
	ft_putchar('\n');
}

void	ft_l_hidden(t_elem *beg)
{
	char *pellol;

	if (beg->hidden == '2')
	{
		ft_putstr_fd("ls: ", 2);
		pellol = (ft_strrchr(beg->lname, '/')) ? \
				((ft_strrchr(beg->lname, '/') + 1)) : (beg->lname);
		ft_putstr_fd(pellol, 2);
		ft_putstr_fd(": Permission denied\n", 2);
		beg->next = NULL;
	}
}

void	ft_l6(t_elem *beg, t_elem *temp, unsigned int great[7], t_elem *r)
{
	if ((beg)->hidden == '0' || (g_flag->a == '1' && beg->hidden != '2'))
	{
		if (g_flag->l == '1')
		{
			ft_l3(beg, temp, great);
			ft_l4(beg, temp, great, r);
			ft_putchar(' ');
			ft_print_time(beg->time);
			ft_putchar(' ');
		}
		ft_l5(beg);
	}
}
