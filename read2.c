/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:35:29 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 15:05:29 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_elem	*ft_close(t_elem *elem, char *path, t_elem *beg, DIR *dire)
{
	if (ft_get_symlnk(path) && g_flag->l == '1')
	{
		elem = ft_new_elemfile(path);
		beg = elem;
	}
	else
		beg = ft_readdir(dire, beg, path, elem);
	if (closedir(dire) == -1)
	{
		perror("closedir");
		exit(1);
	}
	return (beg);
}

t_elem	*ft_opendir(char *path, t_elem *elem, t_elem *beg)
{
	DIR *dire;

	if ((dire = opendir(path)) == NULL)
	{
		if (errno != ENOTDIR)
		{
			if (errno == EACCES)
				return (ft_elem_error(path));
			else if ((errno == ENOENT || errno == EBADF) && ft_link(path) == -1)
				return (NULL);
			else if (ft_get_symlnk(path) && path[ft_strlen(path) - 1] != '/')
				elem = ft_new_elemfile(path);
		}
		else
		{
			if (ft_get_symlnk(path))
				elem = ft_new_elemfile(path);
			if (path[ft_strlen(path) - 1] == '/')
				return (NULL);
			else
				elem = ft_new_elemfile(path);
		}
		beg = elem;
	}
	return (elem == NULL) ? ft_close(elem, path, beg, dire) : (beg);
}
