/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls6.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:20:17 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 16:17:06 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_main_folders(t_elem *folders, t_elem *start, int acc)
{
	t_elem *temp;

	temp = NULL;
	if (folders)
	{
		start = folders;
		if (g_flag->t == '0')
			start = ft_merge_sort(folders, ft_alphadir);
		else
			start = ft_merge_sort(folders, ft_tttime2);
		if (g_flag->r == '1' && start->hidden != '2')
			start = ft_reverse(start);
		while (start)
		{
			temp = start;
			if (g_flag->r == '1' && temp->hidden != '2')
				temp = ft_reversenext(temp);
			temp = ft_ls_l(temp, acc);
			start = start->nextarg;
			if (start)
				ft_putchar('\n');
		}
	}
}

void	ft_openerror(t_error *list)
{
	t_error	*del;

	while (list)
	{
		if (ft_strcmp("", list->path) == 0)
		{
			ft_putstr_fd("ls: fts_open: ", 2);
			opendir(list->path);
			perror(list->path);
			exit(1);
		}
		ft_putstr_fd("ls: ", 2);
		opendir(list->path);
		perror(list->path);
		free(list->path);
		del = list;
		list = list->next;
		free(del);
	}
}

t_error	*ft_readerror(t_error *list, char *str)
{
	t_error *node;

	node = malloc(sizeof(t_error));
	node->path = ft_strdup(str);
	node->next = NULL;
	list = ft_merge_error(list, node, ft_alphaerror);
	return (list);
}

t_elem	*ft_begsort(t_elem *beg)
{
	if (beg->hidden != '2')
	{
		if (g_flag->t == '0')
			beg = ft_merge_sortnext(beg, ft_alpha);
		else if (g_flag->t == '1')
			beg = ft_merge_sortnext(beg, ft_tttime);
	}
	return (beg);
}

void	ft_list_elem(t_elem *start, t_elem *beg)
{
	t_elem *temp;

	if (start == NULL)
		start = beg;
	else
	{
		temp = start;
		while (temp->nextarg)
			temp = temp->nextarg;
		temp->nextarg = beg;
	}
}
