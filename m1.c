/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m1.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:34:33 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/04 13:10:30 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_tttime2(t_elem *elem1, t_elem *elem2)
{
	if (*(ft_timefolder(elem1)) < *(ft_timefolder(elem2)))
		return (1);
	else if (*(ft_timefolder(elem1)) == *(ft_timefolder(elem2)))
		return (ft_strcmp(elem1->lname, elem2->lname));
	else
		return (0);
}

int		ft_filedir(t_elem *elem1, t_elem *elem2)
{
	if (elem1->next != NULL && elem2->next == NULL)
		return (1);
	else
		return (0);
}

t_elem	*ft_getmiddle(t_elem *head)
{
	t_elem	*slow;
	t_elem	*fast;

	slow = head;
	fast = head;
	if (head == NULL)
		return (head);
	while (fast->nextarg != NULL && fast->nextarg->nextarg != NULL)
	{
		slow = slow->nextarg;
		fast = fast->nextarg->nextarg;
	}
	return (slow);
}
