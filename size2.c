/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   size2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 15:40:13 by pguzman           #+#    #+#             */
/*   Updated: 2016/02/01 15:40:52 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int				ft_size_greatest_str(t_elem *temp, int ns)
{
	unsigned int	i;
	t_elem			*t;
	char			*com;

	t = temp;
	i = 0;
	while (t)
	{
		com = (ns == 1) ? t->user_name : t->group_name;
		if (i < ft_strlen(com))
			i = ft_strlen(com);
		t = t->next;
	}
	return (i);
}

void			ft_get_spaces_str(t_elem *te, char *str, int ns, unsigned int a)
{
	int		i;
	t_elem	*t;

	t = te;
	if (a == 0)
		i = ft_size_greatest_str(t, ns) - ft_strlen(str);
	else
		i = a - ft_strlen(str);
	while (i > 0)
	{
		ft_putchar(' ');
		i--;
	}
}
