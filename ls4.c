/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls4.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Updated: 2016/02/04 13:45:47 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_elem	*ft_ls_l(t_elem *beg, int elem)
{
	t_elem				*temp;
	t_elem				*r;
	t_elem				*del;
	static unsigned int great[7];

	temp = beg;
	r = beg;
	ft_l1(great, beg, temp);
	ft_l2(beg, elem, temp);
	while (beg)
	{
		del = beg;
		ft_l6(beg, temp, great, r);
		ft_l_hidden(beg);
		beg = (beg)->next;
	}

	if (g_flag->rr == '1' && r->hidden != '2')
	{
		ft_r(r);
		while(r)
		{
			del = r;
			r = r->next;
			free(del);
		}
	}
	else if (r->hidden != '2')
	{
		while (r)
		{
			del = r;
			free(del->elem_name);
			free(del->lname);
			free(del->permissions);
			free(del->time);
			(del->symlink_name) ? free(del->symlink_name) : NULL;
			r = r->next;
			free(del);
		}
	}
	return (temp);
}

char	ft_set_flags(char c)
{
	if (c == 'l')
		g_flag->l = '1';
	else if (c == 'R')
		g_flag->rr = '1';
	else if (c == 'a')
		g_flag->a = '1';
	else if (c == 'r')
		g_flag->r = '1';
	else if (c == 't')
		g_flag->t = '1';
	else if (c == '1')
		g_flag->one = '1';
	else
		return ('0');
	return ('1');
}

void	ft_start_flags(void)
{
	g_flag = malloc(sizeof(t_flag));
	g_flag->l = '0';
	g_flag->rr = '0';
	g_flag->a = '0';
	g_flag->r = '0';
	g_flag->t = '0';
}

void	ft_inf(char c)
{
	if (ft_set_flags(c) == '0')
	{
		ft_putstr_fd("ls: illegal option -- ", 2);
		ft_putchar_fd(c, 2);
		ft_putstr_fd("\nusage: ls [-Ralrt] [file ...]\n", 2);
		exit(EXIT_FAILURE);
	}
}

int		ft_flags(int ac, char **av)
{
	int	i;
	int j;
	int len;

	i = 1;
	while (i < ac)
	{
		len = ft_strlen(av[i]);
		if (ft_strcmp("--", av[i]) == 0 && av[i + 1] != NULL)
			return (i + 1);
		else if (ft_strcmp("--", av[i]) == 0 && av[i + 1] == NULL)
			return (i);
		else if (ft_strcmp("-", av[i]) == 0)
			return (i);
		if (av[i][0] == '-' && av[i][1])
		{
			j = 1;
			while (j < len)
				ft_inf(av[i][j++]);
		}
		else
			break ;
		i++;
	}
	return (i == ac) ? (i - 1) : (i);
}
